package com.kensoftph.filechooser;

import java.io.File;
import java.util.HashSet;
import java.util.Set;

public class FormFiles {

    private File idDoc;

    private File incomeProofDoc;

    private File assetBackupDoc;

    private Set<String> fileNames;

    public FormFiles() {
        fileNames = new HashSet<>();
    }

    public File getIdDoc() {
        return idDoc;
    }

    public boolean setIdDoc(File idDoc) {
        if (!fileNames.contains(idDoc.getName())) {
            this.idDoc = idDoc;
            fileNames.add(idDoc.getName());
            return true;
        }
        return false;
    }

    public File getIncomeProofDoc() {
        return incomeProofDoc;
    }

    public boolean setIncomeProofDoc(File incomeProofDoc) {
        if (!fileNames.contains(incomeProofDoc.getName())) {
            this.incomeProofDoc = incomeProofDoc;
            fileNames.add(incomeProofDoc.getName());
            return true;
        }
        return false;
    }

    public File getAssetBackupDoc() {
        return assetBackupDoc;
    }

    public boolean setAssetBackupDoc(File assetBackupDoc) {
        if (!fileNames.contains(assetBackupDoc.getName())) {
            this.assetBackupDoc = assetBackupDoc;
            fileNames.add(assetBackupDoc.getName());
            return true;
        }
        return false;
    }

}
