package com.kensoftph.filechooser;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.net.URL;
import java.sql.Blob;
import java.sql.SQLException;
import java.util.ResourceBundle;
import java.util.UUID;
import javax.sql.rowset.serial.SerialBlob;

import javax.sql.rowset.serial.SerialBlob;

import com.kensoftph.filechooser.FormFiles;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonType;
import javafx.stage.FileChooser;
import javafx.stage.Stage;



public class AttachFiles implements Initializable {

    /** Mensaje asociado a la solicitud. */
    @FXML
    private Button btnAssetDoc;

    /** Mensaje asociado a la solicitud. */
    @FXML
    private Button btnIdDoc;

    @FXML
    private Button btnIncomeDoc;

    private String filePath = System.getProperty("user.dir");

    private FileChooser fileChooser = new FileChooser();

    private FormFiles files = new FormFiles();



    /** Mensaje asociado a la solicitud. */
    private Alert errorMsg = new Alert(Alert.AlertType.ERROR);

    /** Mensaje asociado a la solicitud. */
    private Alert infoMsg = new Alert(Alert.AlertType.INFORMATION);


    private void informationMessage(String msg){
        infoMsg.setTitle("Information");
        infoMsg.setContentText(msg);
        infoMsg.showAndWait();
    }

    private void errorMessage(String msg){
        errorMsg.setTitle("Error");
        errorMsg.setContentText(msg);
        errorMsg.setHeaderText("Error Alert");
        errorMsg.showAndWait();
    }

    @FXML
    void getAssetDoc(ActionEvent event) {
        final File file = chosseFile();
        if (file != null) {
            if (!files.setAssetBackupDoc(file)) {
                errorMessage("El archivo ya ha sido ingresado.");
            } else {
                btnAssetDoc.setText(file.getName());
            }
        }
    }

    @FXML
    void getIdDoc(ActionEvent event) {
        final File file = chosseFile();
        if (file != null) {
            if (!files.setIdDoc(file)) {
                errorMessage("El archivo ya ha sido ingresado.");
            } else {
                btnIdDoc.setText(file.getName());
            }
        }
    }

    @FXML
    void getIncomeDoc(ActionEvent event) {
        final File file = chosseFile();
        if (file != null) {
            if (!files.setIncomeProofDoc(file)) {
                errorMessage("El archivo ya ha sido ingresado.");
            } else {
                btnIncomeDoc.setText(file.getName());
            }
        }
    }

    @FXML
    void submitFiles(ActionEvent event) {
        if (files.getIdDoc()==null ||
                files.getAssetBackupDoc()==null ||
                files.getIncomeProofDoc()==null) {
            errorMessage("Tienes que ingresar todos los archivos, obligatoriamente");
        }else{
            showConfirmationDialog();
        }
    }


    private File chosseFile(){
        final Stage stage = (Stage) btnIdDoc.getScene().getWindow();
        final File selectedFile = fileChooser.showOpenDialog(stage);
        if (selectedFile != null) {
            if (selectedFile.length() <= 5 * 1024 * 1024) {
                return selectedFile;
            } else {
                errorMessage("El tamaño del archivo excede los 5MB permitidos.");
            }
        } else {
            errorMessage("Debe adjuntar un archivo");
        }
        return null;
    }

    private void showConfirmationDialog(){
        final Alert confirmation = new Alert(Alert.AlertType.CONFIRMATION);
        confirmation.setTitle("Confirmación de archivos");
        confirmation.setHeaderText("¿Estás seguro de que estos son los archivos a enviar?");
        final StringBuilder contextText = new StringBuilder("Archivos adjuntados: ");
        contextText.append("\nDocumento de identificación " + "=").append(files.getIdDoc().getName());
        contextText.append("\nComprobante de ingresos " + "=").append(files.getIncomeProofDoc().getName());
        contextText.append("\nRespaldo de bienes " + "=").append(files.getAssetBackupDoc().getName());
        confirmation.setContentText(contextText.toString());
        confirmation.setContentText(contextText.toString());
        confirmation.showAndWait().ifPresent(response ->{
            if(response== ButtonType.OK){
                informationMessage("Archivos cargados exitosamente");

                btnAssetDoc.setText("Adjuntar");
                btnIdDoc.setText("Adjuntar");
                btnIncomeDoc.setText("Adjuntar");
                files = null;
            }
        });
    }

    private Blob convertFile(File file) {
        Blob blob = null;
        try {
            final FileInputStream fis = new FileInputStream(file);
            final byte[] bytes = new byte[(int) file.length()];
            fis.read(bytes);
            blob = new SerialBlob(bytes);
        } catch (IOException | SQLException e) {
            e.printStackTrace();
        }
        return blob;
    }

    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {
        fileChooser.setTitle("Selecciona un archivo");
        fileChooser.setInitialDirectory(new File(filePath));
        fileChooser.getExtensionFilters().addAll(
                new FileChooser.ExtensionFilter("PDF Files", "*.pdf")
        );
    }

}
